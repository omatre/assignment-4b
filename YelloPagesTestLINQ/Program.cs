﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace YelloPagesTestLINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Person> people = PopulatePeople(
                    "John Doe",
                    "Sue Storm",
                    "Petter Nordthug",
                    "Henrik Nordtug",
                    "Saint Peter"
                );

            string searchString = GetSearchString();

            SearchPeople(searchString, people);
        }

        private static void SearchPeople(string searchString, List<Person> people)
        {
            IEnumerable<Person> matches = from individual in people
                                          where individual.Fullname
                                                          .ToLower()
                                                          .Contains(searchString.ToLower())
                                                || individual.Fullname
                                                             .ToLower()
                                                             .Equals(searchString.ToLower())
                                          select individual;

            foreach (var match in matches)
            {
                Console.WriteLine(match.Fullname);
            }
        }

        private static string GetSearchString()
        {
            Console.WriteLine("Search names...");

            return Console.ReadLine();
        }

        private static List<Person> PopulatePeople(params string [] names)
        {
            List<Person> people = new List<Person>();

            foreach (var name in names)
            {
                var nameSplit = name.Split();
                people.Add(new Person(nameSplit[0], nameSplit[1]));
            }

            return people;
        }

        struct Person
        {
            public string Firstname;
            public string Lastname;
            public string Fullname;

            public Person(string firstname, string lastname)
            {
                Firstname = firstname;
                Lastname = lastname;
                Fullname = $"{ firstname } { lastname }";
            }
        }
    }
}
